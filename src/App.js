import React from 'react';
import { Routes ,Route } from 'react-router-dom'

import {MonthStatistics} from "./pages/MonthStatistics/components/MonthStatistics";
import {Home} from "./pages/Home/components/Home";
import {Header} from "./Shared/Header/Header";
import {Popup} from "./Shared/Popup/Popup";

function App() {
  return (
      <div className="global-container">
          {/*<Popup /> */}
          <div className="container">
              <Header />
              <Routes>
                  <Route path="/" exact element={<Home />}/>
                  <Route path="/month-statistics" exact element={<MonthStatistics />}/>
              </Routes>
          </div>
      </div>
  )
}

export default App;
