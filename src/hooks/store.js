import {useDispatch, useSelector} from "react-redux";

export const useCustomDispath = () => useDispatch()
export const useCustomSelector = useSelector;